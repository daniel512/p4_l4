﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P4_L4
{
    public abstract class Computer
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}

    