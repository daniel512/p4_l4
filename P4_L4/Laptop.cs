﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P4_L4
{
    public class Laptop : Computer
    {
        public string Manufacturer { get; set; }
        public int Weight { get; set; }
    }

}
