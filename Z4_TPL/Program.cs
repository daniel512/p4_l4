﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Z4_TPL
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var stopwatch = new System.Diagnostics.Stopwatch();
            var google = new WebSite("http://google.pl", "/");
            var ath = new WebSite("http://ath.pl", "/");
            var wiki = new WebSite("http://pl.wikipedia.org", "/wiki/.Net_Core");
            var youtube = new WebSite("http://youtube.com", "/");
            var onet = new WebSite("http://onet.pl", "/");

            var tasks = new List<Task<string>>();

            stopwatch.Start();
            tasks.Add(google.Download());
            Console.WriteLine(stopwatch.Elapsed);
            tasks.Add(ath.Download());
            Console.WriteLine(stopwatch.Elapsed);
            tasks.Add(wiki.Download());
            Console.WriteLine(stopwatch.Elapsed);
            tasks.Add(youtube.Download());
            Console.WriteLine(stopwatch.Elapsed);
            tasks.Add(onet.Download());
            Console.WriteLine(stopwatch.Elapsed);

            Console.WriteLine("-------------------------");

            await Task.WhenAny(tasks);
            Console.WriteLine(stopwatch.Elapsed);
            var htmls = await Task.WhenAll(tasks);
            Console.WriteLine(stopwatch.Elapsed);

            foreach (var html in htmls)
            {
                Console.WriteLine(html.Length);
            }
        }

    }
}
